import {
  html, fixture, assert, fixtureCleanup,
} from '@open-wc/testing';
import '../cells-card-panel.js';

suite('CellsCardPanel', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-card-panel></cells-card-panel>`);
    await el.updateComplete;
  });

  test('Instantiating component', () => {
    const element = el.shadowRoot.querySelector('main');
    assert.isNotNull(element);
  
  });

  test('Test function isExpand', () => {
    assert.isNotNull(el.isExpand());
  });

  test('Test function collapsedPanel(true)', () => {
    el.collapsible = true;
    el.collapsedPanel();
  });

  test('Test function collapsedPanel(false)', () => {
    el.collapsible = false;
    el.collapsedPanel();
  });

  test('Test function actionButtonHandler', () => {
    let div = el.shadowRoot.querySelector('div');
    const evt = {
      stopPropagation: function() {},
      clientX: 10,
      clientY: 10,
      target: div
    }
    el.actionButtonHandler(evt);
  });

});
