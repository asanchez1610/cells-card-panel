import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import './css/demo-styles.js';
import '../cells-card-panel.js';
import '@bbva-commons-web-components/cells-data-table/cells-data-table.js';
import '@bbva-commons-web-components/cells-dropdown-menu/cells-dropdown-menu.js';

// Include below here your components only for demo
// import 'other-component.js'
